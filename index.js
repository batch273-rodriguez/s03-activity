
/*
    Activity 1 - Quiz
    1. Class
    2. class NameOfClass {}
    3. new
    4. instantiate
    5. constructor

    Activity 2 - Quiz
    1. No
    2. No
    3. Yes
    4. dot notation
    5. return the object, or return this
*/

// Activity - Function Coding

class Student {

    constructor(name,email,grades){
        // this.key = value/parameter
        this.name = name;
        this.email = email;
        this.gradeAve = undefined;
        this.passed = undefined;
        this.passedWithHonors = undefined;

        if (typeof grades === 'object' && grades.length === 4) {
            grades.every(grade => grade>=0 && grade<=100)
            ?
                this.grades=grades
            :
                this.grades=undefined;
        } else {
            this.grades = undefined;
        }
    }

    //Methods
    login() {
        console.log(`${this.email} has logged in`);
        return this;
    }

    logout() {
        console.log(`${this.email} has logged out`);
        return this;
    }

    listGrades() {
        console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
        return this;
    }

    computeAve() {
        let sum = 0;
        this.grades.forEach(grade => sum = sum + grade);
        // update property
        this.gradeAve = sum/this.grades.length;
        // return object
        return this;
    }

    willPass() {
        this.passed = this.gradeAve >= 85 ? true : false;
        return this;
    }

    willPassWithHonors() {
        if (this.passed) {
            if (this.gradeAve >= 90) {
                this.passedWithHonors = true;
            } else {
                this.passedWithHonors = false;
            }
        } else {
            this.passedWithHonors = undefined;
        }
        return this;
    }

}

let studentOne = new Student('John','john@mail.com',[89, 84, 78, 88]);
let studentTwo = new Student('Joe','joe@mail.com',[78, 82, 79, 85]);
let studentThree = new Student('Jane','jane@mail.com',[87, 89, 91, 93]);
let studentFour = new Student('Jessie','jessie@mail.com',[91, 89, 92, 93]);

let studentFive = new Student('Above100','above@mail.com',[101, 89, 92, 93]);
let studentSix = new Student('Below0','below@mail.com',[-10, 89, 92, 93]);


console.log(studentOne)
console.log(studentTwo)
console.log(studentThree)
console.log(studentFour)

console.log(studentFive)
console.log(studentSix)

